#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {
    // File pointer to handle file operations
    FILE *file_ptr;

    // Count occurrences of "the" and "a"
    int the_counter = 0;
    int a_counter = 0;
    int total_counter = 0;
    char word[24];

    // Open the file for reading
    file_ptr = fopen("Harry_Potter.txt", "r");
    
    while (fscanf(file_ptr, "%s", word) != -1) {   
        // Check if the word is "the" or "a" for both upper and lower case
        total_counter++;
        if (strcmp(word, "the") == 0) {
            the_counter++;
        } else if (strcmp(word, "The") == 0) {
            the_counter++;
        }else if (strcmp(word, "a") == 0) {
            a_counter++;
        }else if (strcmp(word, "A") == 0) {
            a_counter++;
        }
    }
    
    // Close the file when done
    fclose(file_ptr);
    
    printf("'The' counter: %i\n", the_counter);
    printf("'A' counter: %i\n", a_counter);
    printf("Total counter: %i\n", total_counter);
    
    return 0; // Return 0 to indicate successful execution
}
